#***************************************************************************
#* SPDX-FileCopyrightText: 2021 S. MANKOWSKI stephane@mankowski.fr
#* SPDX-FileCopyrightText: 2021 G. DE BURE support@mankowski.fr
#* SPDX-License-Identifier: GPL-3.0-or-later
#***************************************************************************
MESSAGE( STATUS "..:: CMAKE SKGBANKMODELER ::..")

PROJECT(SKGBANKMODELER)
IF(SKG_DBUS)
    MESSAGE( STATUS "     DBUS enabled")
    ADD_DEFINITIONS(-DSKG_DBUS=${SKG_DBUS})
ELSE(SKG_DBUS)
    MESSAGE( STATUS "     DBUS disabled")
ENDIF(SKG_DBUS)

LINK_DIRECTORIES (${LIBRARY_OUTPUT_PATH})

SET(skgbankmodeler_SRCS
   skgbankobject.cpp
   skgbudgetobject.cpp
   skgbudgetruleobject.cpp
   skgaccountobject.cpp
   skgoperationobject.cpp
   skgrecurrentoperationobject.cpp
   skgtrackerobject.cpp
   skgpayeeobject.cpp
   skgsuboperationobject.cpp
   skgcategoryobject.cpp
   skgunitobject.cpp
   skgunitvalueobject.cpp
   skgruleobject.cpp
   skginterestobject.cpp
   skgdocumentbank.cpp
   skgimportexportmanager.cpp
   skgimportplugin.cpp
   skgreportbank.cpp
 )

#build a shared library
ADD_LIBRARY(skgbankmodeler SHARED ${skgbankmodeler_SRCS})

#need to link to some other libraries ? just add them here
TARGET_LINK_LIBRARIES(skgbankmodeler LINK_PUBLIC KF5::Parts Qt5::Xml skgbasemodeler)
SET_TARGET_PROPERTIES( skgbankmodeler PROPERTIES VERSION ${SKG_VERSION} SOVERSION ${SOVERSION} )

GENERATE_EXPORT_HEADER(skgbankmodeler BASE_NAME skgbankmodeler)

ADD_SUBDIRECTORY(currency)

########### install files ###############
INSTALL(TARGETS skgbankmodeler ${INSTALL_TARGETS_DEFAULT_ARGS}  LIBRARY NAMELINK_SKIP)
INSTALL(FILES ${PROJECT_SOURCE_DIR}/org.kde.skrooge-import-plugin.desktop DESTINATION ${KDE_INSTALL_KSERVICETYPES5DIR})
INSTALL(FILES ${PROJECT_SOURCE_DIR}/org.kde.skrooge-source-plugin.desktop DESTINATION ${KDE_INSTALL_KSERVICETYPES5DIR})
INSTALL(DIRECTORY sources DESTINATION ${KDE_INSTALL_KSERVICES5DIR} FILES_MATCHING PATTERN "*.desktop")
INSTALL(PROGRAMS skrooge-yahoodl.py DESTINATION ${KDE_INSTALL_DATADIR}/skrooge)
INSTALL(PROGRAMS skrooge-ratesapi.py DESTINATION ${KDE_INSTALL_DATADIR}/skrooge)
INSTALL(PROGRAMS skrooge-cryptocompare.py DESTINATION ${KDE_INSTALL_DATADIR}/skrooge)
INSTALL(PROGRAMS skrooge-coinmarketcap.py DESTINATION ${KDE_INSTALL_DATADIR}/skrooge)
INSTALL(PROGRAMS skrooge-exchangerates.py DESTINATION ${KDE_INSTALL_DATADIR}/skrooge)
