/***************************************************************************
 * SPDX-FileCopyrightText: 2021 S. MANKOWSKI stephane@mankowski.fr
 * SPDX-FileCopyrightText: 2021 G. DE BURE support@mankowski.fr
 * SPDX-License-Identifier: GPL-3.0-or-later
 ***************************************************************************/
#ifndef SKROOGEAKONADIRESOURCE_H
#define SKROOGEAKONADIRESOURCE_H

#include <akonadi/resourcebase.h>

class skroogeakonadiResource : public Akonadi::ResourceBase,
    public Akonadi::AgentBase::Observer
{
    Q_OBJECT

public:
    explicit skroogeakonadiResource(const QString& id);
    ~skroogeakonadiResource();

public Q_SLOTS:
    virtual void configure(WId windowId);

protected Q_SLOTS:
    void retrieveCollections();
    void retrieveItems(const Akonadi::Collection& collection);
    bool retrieveItem(const Akonadi::Item& item, const QSet<QByteArray>& parts);

    void refresh();
};

#endif
