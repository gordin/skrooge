#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#***************************************************************************
#* SPDX-FileCopyrightText: 2021 S. MANKOWSKI stephane@mankowski.fr
#* SPDX-FileCopyrightText: 2021 G. DE BURE support@mankowski.fr
#* SPDX-License-Identifier: GPL-3.0-or-later
#***************************************************************************
# This script is based on l10n-fetch-po-files.py from GCompris and written by Trijita org <jktjkt@trojita.org>
import os
import re
import subprocess
import shutil

print("Downloading po. Please wait this can take time...")
SVN_PATH = "svn://anonsvn.kde.org/home/kde/trunk/l10n-kf5/"
SOURCE_PO_PATH = ["/messages/skrooge/skrooge.po", "/docs/skrooge/skrooge/index.docbook"]
OUTPUT_PO_PATH = "./po/"

fixer = re.compile(r'^#~\| ', re.MULTILINE)
re_empty_msgid = re.compile('^msgid ""$', re.MULTILINE)
re_empty_line = re.compile('^$', re.MULTILINE)
re_has_qt_contexts = re.compile('X-Qt-Contexts: true\\n')

if not os.path.exists(OUTPUT_PO_PATH):
    os.mkdir(OUTPUT_PO_PATH)

all_languages = subprocess.check_output(['svn', 'cat', SVN_PATH + 'subdirs'],
                                       stderr=subprocess.STDOUT).decode('utf-8')
all_languages = all_languages.split('\n')
all_languages.remove("x-test")
all_languages.remove("")
for lang in all_languages:
    try:
        lang_path = os.path.join(OUTPUT_PO_PATH, lang)
        if os.path.exists(lang_path):
            shutil.rmtree(lang_path)
        os.mkdir(lang_path)   
        
        for stp in SOURCE_PO_PATH:
            file_name = os.path.basename(stp)
            cmd = ['svn', 'cat', SVN_PATH + lang + stp]
            raw_data = subprocess.check_output(cmd, stderr=subprocess.PIPE).decode('utf-8')
            if file_name == "index.docbook":
                os.mkdir(os.path.join(lang_path, "docs"))
                os.mkdir(os.path.join(lang_path, "docs/skrooge"))
                
                file=open(os.path.join(os.path.join(lang_path, "docs/skrooge"), os.path.basename(stp)), "w")
                
                transformed = raw_data
                print("Fetched {}".format(lang))
            else:
                file=open(os.path.join(lang_path, os.path.basename(stp)), "w")
                
                (transformed, subs) = fixer.subn('# ~| ', raw_data)
                pos1 = re_empty_msgid.search(transformed).start()
                pos2 = re_empty_line.search(transformed).start()
                if re_has_qt_contexts.search(transformed, pos1, pos2) is None:
                    transformed = transformed[:pos2] + \
                            '"X-Qt-Contexts: true\\n"\n' + \
                            transformed[pos2:]
                    subs = subs + 1
                if (subs > 0):
                    print("Fetched {} (and performed {} cleanups)".format(lang, subs))
                else:
                    print("Fetched {}".format(lang))
                

        
            
            file.write(transformed)
            file.close
            print(file.name+" created")
    except subprocess.CalledProcessError:
        print("No data for {}".format(lang))
