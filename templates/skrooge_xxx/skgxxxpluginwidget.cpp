/***************************************************************************
 * SPDX-FileCopyrightText: 2021 S. MANKOWSKI stephane@mankowski.fr
 * SPDX-FileCopyrightText: 2021 G. DE BURE support@mankowski.fr
 * SPDX-License-Identifier: GPL-3.0-or-later
 ***************************************************************************/
/** @file
 * ##DESCRIPTION##.
 *
 * @author ##AUTHOR##
 */
#include "skgxxxpluginwidget.h"

#include <qdom.h>

#include "skgmainpanel.h"
#include "skgtraces.h"
#include "skgdocument.h"

SKGXXXPluginWidget::SKGXXXPluginWidget(QWidget* iParent, SKGDocument* iDocument)
    : SKGTabPage(iParent, iDocument)
{
    SKGTRACEINFUNC(1)
    if (!iDocument) {
        return;
    }

    ui.setupUi(this);

    // Build you panel here
}

SKGXXXPluginWidget::~SKGXXXPluginWidget()
{
    SKGTRACEINFUNC(1)
}

QString SKGXXXPluginWidget::getState()
{
    SKGTRACEINFUNC(10)
    QDomDocument doc("SKGML");
    QDomElement root = doc.createElement("parameters");
    doc.appendChild(root);

    // Get state
    // Example: QString account=root.attribute ( "account");
    //         if (account.isEmpty()) root.setAttribute(QStringLiteral("account"), ui.kDisplayAccountCombo->currentText());

    return doc.toString();
}

void SKGXXXPluginWidget::setState(const QString& iState)
{
    SKGTRACEINFUNC(10)
    QDomDocument doc("SKGML");
    doc.setContent(iState);
    QDomElement root = doc.documentElement();

    // Set state
    // Example: QString account=root.attribute ( "account");
}

QString SKGXXXPluginWidget::getDefaultStateAttribute()
{
    return "";
}

QWidget* SKGXXXPluginWidget::mainWidget()
{
    return this;
}

void SKGXXXPluginWidget::refresh()

{
    SKGTRACEINFUNC(1)

    QSqlDatabase* db = getDocument()->getDatabase();
    setEnabled(db != nullptr);
    if (db != nullptr) {
        // Refresh yours widgets here
    }
}




