/***************************************************************************
 * SPDX-FileCopyrightText: 2021 S. MANKOWSKI stephane@mankowski.fr
 * SPDX-FileCopyrightText: 2021 G. DE BURE support@mankowski.fr
 * SPDX-License-Identifier: GPL-3.0-or-later
 ***************************************************************************/
#ifndef SKGXXXPLUGIN_H
#define SKGXXXPLUGIN_H
/** @file
 * ##DESCRIPTION##.
 *
 * @author ##AUTHOR##
 */
#include "skginterfaceplugin.h"
#include "ui_skgxxxpluginwidget_pref.h"


/**
 * ##DESCRIPTION##
 */
class SKGXXXPlugin : public SKGInterfacePlugin
{
    Q_OBJECT
    Q_INTERFACES(SKGInterfacePlugin)

public:
    /**
     * Default Constructor
     */
    explicit SKGXXXPlugin(QWidget* iWidget, QObject* iParent, const QVariantList& iArg);

    /**
     * Default Destructor
     */
    virtual ~SKGXXXPlugin();

    /**
     * Called to initialise the plugin
     * @param iDocument the main document
     * @return true if the plugin is compatible with the document
     */
    virtual bool setupActions(SKGDocument* iDocument) override;

    /**
     * Must be modified to refresh widgets after a modification.
     */
    virtual void refresh() override;

    /**
    * The preference widget of the plugin.
    * @return The preference widget of the plugin
     */
    virtual QWidget* getPreferenceWidget() override;

    /**
     * The preference skeleton of the plugin.
     * @return The preference skeleton of the plugin
     */
    virtual KConfigSkeleton* getPreferenceSkeleton() override;

    /**
     * The page widget of the plugin.
     * @return The page widget of the plugin
     */
    virtual SKGTabPage* getWidget() override;

    /**
     * This function is called when preferences have been modified. Must be used to save some parameters into the document.
     * A transaction is already opened
     * @return an object managing the error.
     *   @see SKGError
     */
    virtual SKGError savePreferences() const override;

    /**
     * The title of the plugin.
     * @return The title of the plugin
     */
    virtual QString title() const override;

    /**
     * The icon of the plugin.
     * @return The icon of the plugin
     */
    virtual QString icon() const override;

    /**
     * The toolTip of the plugin.
     * @return The toolTip of the plugin
     */
    virtual QString toolTip() const override;

    /**
     * The tips list of the plugin.
     * @return The tips list of the plugin
     */
    virtual QStringList tips() const override;

    /**
     * Must be implemented to set the position of the plugin.
     * @return integer value between 0 and 999 (default = 999)
     */
    virtual int getOrder() const override;

    /**
     * Must be implemented to know if a plugin must be display in pages chooser.
     * @return true of false (default = false)
     */
    virtual bool isInPagesChooser() const override;

private Q_SLOTS:

private:
    Q_DISABLE_COPY(SKGXXXPlugin)

    SKGDocument* m_currentDocument;

    Ui::skgxxxplugin_pref ui;
};

#endif  // SKGXXXPLUGIN_H
