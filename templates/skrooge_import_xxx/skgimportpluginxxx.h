/***************************************************************************
 * SPDX-FileCopyrightText: 2021 S. MANKOWSKI stephane@mankowski.fr
 * SPDX-FileCopyrightText: 2021 G. DE BURE support@mankowski.fr
 * SPDX-License-Identifier: GPL-3.0-or-later
 ***************************************************************************/
#ifndef SKGIMPORTPLUGINXXX_H
#define SKGIMPORTPLUGINXXX_H
/** @file
* This file is Skrooge plugin for XXX import / export.
*
* @author Stephane MANKOWSKI / Guillaume DE BURE
*/
#include "skgimportplugin.h"

/**
 * This file is Skrooge plugin for XXX import / export.
 */
class SKGImportPluginXXX : public SKGImportPlugin
{
    Q_OBJECT
    Q_INTERFACES(SKGImportPlugin);

public:
    /**
     * Default constructor
     * @param iImporter the parent importer
     * @param iArg the arguments
     */
    explicit SKGImportPluginXXX(QObject* iImporter, const QVariantList& iArg);

    /**
     * Default Destructor
     */
    virtual ~SKGImportPluginXXX();

    /**
     * To know if import is possible with this plugin
     */
    virtual bool isImportPossible() override;

    /**
     * Import a file
     * @return an object managing the error.
     *   @see SKGError
     */
    virtual SKGError importFile() override;

    /**
     * To know if export is possible with this plugin
     * @return true or false
     */
    virtual bool isExportPossible() override;


    /**
     * Export a file
     * @return an object managing the error.
     *   @see SKGError
     */
    virtual SKGError exportFile() override;

    /**
     * Return the mime type filter
     * @return the mime type filter. Example: "*.csv|CSV file"
     */
    virtual QString getMimeTypeFilter() const override;


private:
    Q_DISABLE_COPY(SKGImportPluginXXX);
};

#endif  // SKGIMPORTPLUGINXXX_H
