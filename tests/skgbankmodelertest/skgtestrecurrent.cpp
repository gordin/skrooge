/***************************************************************************
 * SPDX-FileCopyrightText: 2021 S. MANKOWSKI stephane@mankowski.fr
 * SPDX-FileCopyrightText: 2021 G. DE BURE support@mankowski.fr
 * SPDX-License-Identifier: GPL-3.0-or-later
 ***************************************************************************/
/** @file
 * This file is a test script.
 *
 * @author Stephane MANKOWSKI / Guillaume DE BURE
 */
#include "skgtestmacro.h"
#include "skgbankincludes.h"
#include "skgservices.h"

/**
 * The main function of the unit test
 * @param argc the number of arguments
 * @param argv the list of arguments
 */
int main(int argc, char** argv)
{
    Q_UNUSED(argc)
    Q_UNUSED(argv)

    // Init test
    SKGINITTEST(true)

    // ============================================================================
    {
        // Test bank document
        SKGDocumentBank document1;
        SKGTESTERROR(QStringLiteral("document1.initialize()"), document1.initialize(), true)
        SKGBankObject bank(&document1);
        SKGAccountObject account;
        SKGUnitObject unit_euro(&document1);
        SKGUnitValueObject unit_euro_val1;
        QDate d1 = QDate::currentDate().addMonths(-6);
        SKGError err;
        {
            // Scope of the transaction
            SKGBEGINTRANSACTION(document1, QStringLiteral("BANK_T1"), err)

            // Creation bank
            SKGTESTERROR(QStringLiteral("BANK:setName"), bank.setName(QStringLiteral("CREDIT COOP")), true)
            SKGTESTERROR(QStringLiteral("BANK:save"), bank.save(), true)

            // Creation account
            SKGTESTERROR(QStringLiteral("BANK:addAccount"), bank.addAccount(account), true)
            SKGTESTERROR(QStringLiteral("ACCOUNT:setName"), account.setName(QStringLiteral("Courant steph")), true)
            SKGTESTERROR(QStringLiteral("ACCOUNT:save"), account.save(), true)

            // Creation unit
            SKGTESTERROR(QStringLiteral("UNIT:setName"), unit_euro.setName(QStringLiteral("euro")), true)
            SKGTESTERROR(QStringLiteral("UNIT:save"), unit_euro.save(), true)

            // Creation unitvalue
            SKGTESTERROR(QStringLiteral("UNIT:addUnitValue"), unit_euro.addUnitValue(unit_euro_val1), true)
            SKGTESTERROR(QStringLiteral("UNITVALUE:setQuantity"), unit_euro_val1.setQuantity(1), true)
            SKGTESTERROR(QStringLiteral("UNITVALUE:setDate"), unit_euro_val1.setDate(d1), true)
            SKGTESTERROR(QStringLiteral("UNITVALUE:save"), unit_euro_val1.save(), true)

            // Creation operation
            SKGOperationObject op;
            SKGTESTERROR(QStringLiteral("ACCOUNT:addOperation"), account.addOperation(op), true)

            SKGObjectBase::SKGListSKGObjectBase recups;
            SKGTESTERROR(QStringLiteral("OP:getRecurrentOperations"), op.getRecurrentOperations(recups), false)

            SKGRecurrentOperationObject recuope;
            SKGTESTERROR(QStringLiteral("OP:addRecurrentOperation"), op.addRecurrentOperation(recuope), false)

            SKGTESTERROR(QStringLiteral("OPE:setMode"), op.setMode(QStringLiteral("cheque")), true)
            SKGTESTERROR(QStringLiteral("OPE:setComment"), op.setComment(QStringLiteral("10 tickets")), true)
            SKGTESTERROR(QStringLiteral("OPE:setDate"), op.setDate(d1), true)
            SKGTESTERROR(QStringLiteral("OPE:setUnit"), op.setUnit(unit_euro), true)
            SKGTESTERROR(QStringLiteral("OPE:save"), op.save(), true)

            {
                SKGSubOperationObject subop;
                SKGTESTERROR(QStringLiteral("OPE:addSubOperation"), op.addSubOperation(subop), true)
                SKGTESTERROR(QStringLiteral("SUBOPE:setQuantity"), subop.setQuantity(8.5), true)
                SKGTESTERROR(QStringLiteral("SUBOPE:save"), subop.save(), true)
            }
            {
                SKGSubOperationObject subop;
                SKGTESTERROR(QStringLiteral("OPE:addSubOperation"), op.addSubOperation(subop), true)
                SKGTESTERROR(QStringLiteral("SUBOPE:setQuantity"), subop.setQuantity(10), true)
                SKGTESTERROR(QStringLiteral("SUBOPE:save"), subop.save(), true)
            }
            // Checks
            SKGTESTERROR(QStringLiteral("OPE:load"), op.load(), true) {
                SKGOperationObject op2;
                SKGTESTERROR(QStringLiteral("ACCOUNT:addOperation"), account.addOperation(op2), true)
                SKGTESTERROR(QStringLiteral("OPE:setMode"), op2.setMode(QStringLiteral("cheque")), true)
                SKGTESTERROR(QStringLiteral("OPE:setComment"), op2.setComment(QStringLiteral("10 tickets")), true)
                SKGTESTERROR(QStringLiteral("OPE:setDate"), op2.setDate(d1), true)
                SKGTESTERROR(QStringLiteral("OPE:setUnit"), op2.setUnit(unit_euro), true)
                SKGTESTERROR(QStringLiteral("OPE:save"), op2.save(), true)

                SKGSubOperationObject subop;
                SKGTESTERROR(QStringLiteral("OPE:addSubOperation"), op2.addSubOperation(subop), true)
                SKGTESTERROR(QStringLiteral("SUBOPE:setQuantity"), subop.setQuantity(8.5), true)
                SKGTESTERROR(QStringLiteral("SUBOPE:save"), subop.save(), true)

                SKGTESTERROR(QStringLiteral("OPE:save"), op.setGroupOperation(op2), true)
                SKGTESTERROR(QStringLiteral("OPE:save"), op.save(), true)
            }

            // Add recurrent operation
            SKGTESTERROR(QStringLiteral("OP:addRecurrentOperation"), op.addRecurrentOperation(recuope), true)
            SKGTESTERROR(QStringLiteral("RECOP:setPeriodIncrement"), recuope.setPeriodIncrement(2), true)
            SKGTESTERROR(QStringLiteral("RECOP:setPeriodUnit"), recuope.setPeriodUnit(SKGRecurrentOperationObject::MONTH), true)
            SKGTESTERROR(QStringLiteral("RECOP:setAutoWriteDays"), recuope.setAutoWriteDays(6), true)
            SKGTESTERROR(QStringLiteral("RECOP:autoWriteEnabled"), recuope.autoWriteEnabled(true), true)
            SKGTESTERROR(QStringLiteral("RECOP:setWarnDays"), recuope.setWarnDays(10), true)
            SKGTESTERROR(QStringLiteral("RECOP:warnEnabled"), recuope.warnEnabled(true), true)
            SKGTESTERROR(QStringLiteral("RECOP:setTimeLimit"), recuope.setTimeLimit(QDate::currentDate().addMonths(-4)), true)
            SKGTESTERROR(QStringLiteral("RECOP:timeLimit"), recuope.timeLimit(true), true)
            SKGTESTERROR(QStringLiteral("RECOP:save"), recuope.save(), true)

            SKGRecurrentOperationObject recuope2(recuope);
            SKGRecurrentOperationObject recuope3(SKGObjectBase(recuope.getDocument(), QStringLiteral("xxx"), recuope.getID()));
            SKGRecurrentOperationObject recuope4(SKGNamedObject(recuope.getDocument(), QStringLiteral("xxx"), recuope.getID()));

            SKGObjectBase::SKGListSKGObjectBase recops;
            SKGTESTERROR(QStringLiteral("OP:getRecurrentOperations"), op.getRecurrentOperations(recops), true)
            SKGTEST(QStringLiteral("RECOP:recops.count"), recops.count(), 1)

            SKGTEST(QStringLiteral("RECOP:getPeriodIncrement"), recuope.getPeriodIncrement(), 2)
            SKGTEST(QStringLiteral("RECOP:getPeriodUnit"), static_cast<unsigned int>(recuope.getPeriodUnit()), static_cast<unsigned int>(SKGRecurrentOperationObject::MONTH))
            SKGTEST(QStringLiteral("RECOP:getAutoWriteDays"), recuope.getAutoWriteDays(), 6)
            SKGTESTBOOL("RECOP:isAutoWriteEnabled", recuope.isAutoWriteEnabled(), true)
            SKGTEST(QStringLiteral("RECOP:getWarnDays"), recuope.getWarnDays(), 10)
            SKGTESTBOOL("RECOP:isWarnEnabled", recuope.isWarnEnabled(), true)
            SKGTEST(QStringLiteral("RECOP:getTimeLimit"), recuope.getTimeLimit(), 3)
            SKGTESTBOOL("RECOP:hasTimeLimit", recuope.hasTimeLimit(), true)
            SKGTEST(QStringLiteral("RECOP:getDate"), recuope.getDate().toString(), d1.toString())

            SKGOperationObject ope2;
            SKGTESTERROR(QStringLiteral("RECOP:getParentOperation"), recuope.getParentOperation(ope2), true)
            SKGTESTBOOL("RECOP:op==ope2", (op == ope2), true)

            document1.dump(DUMPOPERATION);

            int nbi = 0;
            SKGTESTERROR(QStringLiteral("RECOP:process"), recuope.process(nbi), true)
            SKGTEST(QStringLiteral("RECOP:nbi"), nbi, 3)
            SKGTESTERROR(QStringLiteral("RECOP:getParentOperation"), recuope.getParentOperation(ope2), true)
            SKGTESTBOOL("RECOP:op!=ope2", (op != ope2), true)

            SKGTESTERROR(QStringLiteral("RECOP:getRecurredOperations"), recuope.getRecurredOperations(recops), true)
            SKGTEST(QStringLiteral("RECOP:recops.count"), recops.count(), 3)

            SKGTESTERROR(QStringLiteral("RECOP:SKGRecurrentOperationObject::process"), SKGRecurrentOperationObject::process(&document1, nbi), true)

            document1.dump(DUMPOPERATION);


            SKGTESTERROR(QStringLiteral("RECOP:setPeriodIncrement"), recuope.setPeriodIncrement(2), true)
            SKGTESTERROR(QStringLiteral("RECOP:setPeriodUnit"), recuope.setPeriodUnit(SKGRecurrentOperationObject::DAY), true)
            recuope.getNextDate();
            SKGTESTERROR(QStringLiteral("RECOP:setPeriodUnit"), recuope.setPeriodUnit(SKGRecurrentOperationObject::WEEK), true)
            recuope.getNextDate();
            SKGTESTERROR(QStringLiteral("RECOP:setPeriodUnit"), recuope.setPeriodUnit(SKGRecurrentOperationObject::MONTH), true)
            recuope.getNextDate();
            SKGTESTERROR(QStringLiteral("RECOP:setPeriodUnit"), recuope.setPeriodUnit(SKGRecurrentOperationObject::YEAR), true)
            recuope.getNextDate();
        }
    }

    // End test
    SKGENDTEST()
}
