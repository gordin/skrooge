#***************************************************************************
#* SPDX-FileCopyrightText: 2021 S. MANKOWSKI stephane@mankowski.fr
#* SPDX-FileCopyrightText: 2021 G. DE BURE support@mankowski.fr
#* SPDX-License-Identifier: GPL-3.0-or-later
#***************************************************************************
MESSAGE( STATUS "..:: CMAKE SKGBASEGUITEST ::..")

PROJECT(SKBBASEMODELERTEST)

IF(SKG_WEBENGINE)
    MESSAGE( STATUS "     Mode WebEngine")
    ADD_DEFINITIONS(-DSKG_WEBENGINE=${SKG_WEBENGINE})
ENDIF(SKG_WEBENGINE)
IF(SKG_WEBKIT)
    MESSAGE( STATUS "     Mode Webkit")
    ADD_DEFINITIONS(-DSKG_WEBKIT=${SKG_WEBKIT})
ENDIF(SKG_WEBKIT)

ADD_DEFINITIONS(-DQT_GUI_LIB)
LINK_DIRECTORIES (${LIBRARY_OUTPUT_PATH})

INCLUDE_DIRECTORIES( ${CMAKE_SOURCE_DIR}/tests/skgbasemodelertest )

#Add test
ENABLE_TESTING()
FILE(GLOB cpp_files "skgtest*.cpp")
LIST(SORT cpp_files)
FOREACH(file ${cpp_files})
    GET_FILENAME_COMPONENT(utname ${file} NAME_WE)
    ADD_EXECUTABLE(${utname} ${file})
    TARGET_LINK_LIBRARIES(${utname} Qt5::Gui Qt5::Core Qt5::Test skgbasegui skgbaseguidesigner skgbasemodeler)
    ADD_TEST(NAME ${utname} COMMAND ${CMAKE_SOURCE_DIR}/tests/scripts/${utname}.sh)
ENDFOREACH()
INCLUDE(CTest)
