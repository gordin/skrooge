#***************************************************************************
#* SPDX-FileCopyrightText: 2021 S. MANKOWSKI stephane@mankowski.fr
#* SPDX-FileCopyrightText: 2021 G. DE BURE support@mankowski.fr
#* SPDX-License-Identifier: GPL-3.0-or-later
#***************************************************************************
MESSAGE( STATUS "..:: CMAKE PLUGIN_OPERATION ::..")

PROJECT(plugin_operation)

LINK_DIRECTORIES (${LIBRARY_OUTPUT_PATH})

SET(skrooge_operation_SRCS
	skgsplittabledelegate.cpp
	skgoperationplugin.cpp
	skgoperationpluginwidget.cpp 
	skgoperationboardwidget.cpp
	skgoperationboardwidgetqml.cpp
)

ki18n_wrap_ui(skrooge_operation_SRCS skgoperationpluginwidget_base.ui skgoperationpluginwidget_pref.ui skgoperationpluginwidget_board.ui )
kconfig_add_kcfg_files(skrooge_operation_SRCS skgoperation_settings.kcfgc )

ADD_LIBRARY(skrooge_operation MODULE ${skrooge_operation_SRCS})
TARGET_LINK_LIBRARIES(skrooge_operation KF5::Parts KF5::ItemViews skgbasemodeler skgbasegui skgbankmodeler skgbankgui)

########### install files ###############
INSTALL(TARGETS skrooge_operation DESTINATION ${KDE_INSTALL_QTPLUGINDIR})
INSTALL(FILES ${PROJECT_SOURCE_DIR}/org.kde.skrooge-plugin-operation.desktop DESTINATION ${KDE_INSTALL_KSERVICES5DIR})
INSTALL(FILES ${PROJECT_SOURCE_DIR}/skrooge_operation.rc  DESTINATION  ${KDE_INSTALL_KXMLGUI5DIR}/skrooge_operation )
INSTALL(FILES ${PROJECT_SOURCE_DIR}/skgoperation_settings.kcfg  DESTINATION  ${KDE_INSTALL_KCFGDIR} )
