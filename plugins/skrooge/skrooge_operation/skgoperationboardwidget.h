/***************************************************************************
 * SPDX-FileCopyrightText: 2021 S. MANKOWSKI stephane@mankowski.fr
 * SPDX-FileCopyrightText: 2021 G. DE BURE support@mankowski.fr
 * SPDX-License-Identifier: GPL-3.0-or-later
 ***************************************************************************/
#ifndef SKGOPERATIONBOARDWIDGET_H
#define SKGOPERATIONBOARDWIDGET_H
/** @file
* This file is Skrooge plugin for operation management.
*
* @author Stephane MANKOWSKI / Guillaume DE BURE
*/
#include <qtimer.h>

#include "skgboardwidget.h"
#include "ui_skgoperationpluginwidget_board.h"

class QAction;
class SKGPeriodEdit;
class QParallelAnimationGroup;

/**
 * This file is Skrooge plugin for operation management
 */
class SKGOperationBoardWidget : public SKGBoardWidget
{
    Q_OBJECT

public:
    /**
     * Default Constructor
     * @param iParent the parent widget
     * @param iDocument the document
     */
    explicit SKGOperationBoardWidget(QWidget* iParent, SKGDocument* iDocument);

    /**
     * Default Destructor
     */
    ~SKGOperationBoardWidget() override;

    /**
     * Get the current state
     * MUST BE OVERWRITTEN
     * @return a string containing all information needed to set the same state.
     * Could be an XML stream
     */
    QString getState() override;

    /**
     * Set the current state
     * MUST BE OVERWRITTEN
     * @param iState must be interpreted to set the state of the widget
     */
    void setState(const QString& iState) override;

private Q_SLOTS:
    void refreshDelayed();
    void dataModified(const QString& iTableName = QString(), int iIdTransaction = 0);
    void onOpen(const QString& iLink);

private:
    Q_DISABLE_COPY(SKGOperationBoardWidget)

    void setValue(SKGProgressBar* iWidget, double iValue);

    Ui::skgoperationplugin_board ui{};

    QAction* m_menuOpen;
    QAction* m_menuGroup;
    QAction* m_menuTransfer;
    QAction* m_menuTracked;
    QAction* m_menuSuboperation;

    SKGPeriodEdit* m_periodEdit1;
    SKGPeriodEdit* m_periodEdit2;

    QParallelAnimationGroup* m_anim;

    QTimer m_timer;
};

#endif  // SKGOPERATIONBOARDWIDGET_H
