<!--
/***************************************************************************
 * SPDX-FileCopyrightText: 2021 S. MANKOWSKI stephane@mankowski.fr
 * SPDX-FileCopyrightText: 2021 G. DE BURE support@mankowski.fr
 * SPDX-License-Identifier: GPL-3.0-or-later
 ***************************************************************************/
-->
<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "https://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="https://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
    <meta http-equiv="Content-Style-Type" content="text/css" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css">
  </head>
  <body>
  <h1>Introduction</h1>
  Welcome to this tutorial. You will learn how to develop a template to personalize your how report.<br>
  The template engine is based on <a href="http://www.grantlee.org/apidox/">Grantlee</a>.<br>
  <br>
  You can find information here:<br>
  <a href="http://www.grantlee.org/apidox/for_themers.html">http://www.grantlee.org/apidox/for_themers.html</a><br>
  <a href="https://docs.djangoproject.com/en/dev/ref/templates/builtins/">https://docs.djangoproject.com/en/dev/ref/templates/builtins/</a><br>

  <h1>The main variables</h1>  
  <h2>The simple values:</h2>
You can have access to simple values:
<div class="panel panel-primary">
  <div class="panel-heading">Code</div>
  <div class="panel-body">
color_negativetext: &#123;{ color_negativetext }}<br/>
color_positivetext: &#123;{ color_positivetext }}<br/>
color_neutraltext: &#123;{ color_neutraltext }}<br/>
color_normaltext: &#123;{ color_normaltext }}<br/>
color_inactivetext: &#123;{ color_inactivetext }}<br/>
color_activetext: &#123;{ color_activetext }}<br/>
color_linktext: &#123;{ color_linktext }}<br/>
color_visitedtext: &#123;{ color_visitedtext }}<br/>
color_activebackground: &#123;{ color_activebackground }}<br/>

font_family: &#123;{ font_family }}<br/>

logo: &#123;{ logo }} &lt;img src="&#123;{ logo }}" /><br/>
logo_black: &#123;{ logo_black }} &lt;img src="&#123;{ logo_black }}" /><br/>

title_main: &#123;{ title_main }}<br/>
title_budget: &#123;{ title_budget }}<br/>
title_main_categories: &#123;{ title_main_categories }}<br/>
title_variations: &#123;{ title_variations }}<br/>
title_account: &#123;{ title_account }}<br/>
title_unit: &#123;{ title_unit }}<br/>
title_advice: &#123;{ title_advice }}<br/>
title_portfolio: &#123;{ title_portfolio }}<br/>
title_highlighted: &#123;{ title_highlighted }}<br/>
title_networth: &#123;{ title_networth }}<br/>
title_annual_spending: &#123;{ title_annual_spending }}<br/>
title_personal_finance_score: &#123;{ title_personal_finance_score }}<br/>

msg_no_variation: &#123;{ msg_no_variation|safe }}<br/>
msg_no_scheduled: &#123;{ msg_no_scheduled|safe }}<br/>
msg_no_highlighted: &#123;{ msg_no_highlighted|safe }}<br/>
msg_no_budget: &#123;{ msg_no_budget|safe }}<br/>
msg_no_share: &#123;{ msg_no_share|safe }}<br/>
msg_amount_unit_date: &#123;{ msg_amount_unit_date|safe }}<br/>

about_welcome: &#123;{ about_welcome|safe }}<br/>
about_programname: &#123;{ about_programname|safe }}<br/>
about_version: &#123;{ about_version|safe }}<br/>
about_bugaddress: &#123;{ about_bugaddress|safe }}<br/>
about_copyrightstatement: &#123;{ about_copyrightstatement|safe }}<br/>
about_homepage: &#123;{ about_homepage|safe }}<br/>
about_forumpage: &#123;{ about_forumpage|safe }}<br/>
about_newspage: &#123;{ about_newspage|safe }}<br/>
about_operationpage: &#123;{ about_operationpage|safe }}<br/>
about_accountpage: &#123;{ about_accountpage|safe }}<br/>
about_shortdescription: &#123;{ about_shortdescription|safe }}<br/>
about_othertext: &#123;{ about_othertext|safe }}<br/>
about_maintext: &#123;{ about_maintext|safe }}<br/>
about_did_you_know: &#123;{ about_did_you_know|safe }}<br/>
  </div>
</div>
  
<div class="panel panel-primary">
  <div class="panel-heading">Result</div>
  <div class="panel-body">
color_negativetext: {{ color_negativetext }}<br/>
color_positivetext: {{ color_positivetext }}<br/>
color_neutraltext: {{ color_neutraltext }}<br/>
color_normaltext: {{ color_normaltext }}<br/>
color_inactivetext: {{ color_inactivetext }}<br/>
color_activetext: {{ color_activetext }}<br/>
color_linktext: {{ color_linktext }}<br/>
color_visitedtext: {{ color_visitedtext }}<br/>
color_activebackground: {{ color_activebackground }}<br/>

font_family: {{ font_family }}<br/>

logo: {{ logo }} <img src="{{ logo }}" /><br/>
logo_black: {{ logo_black }} <img src="{{ logo_black }}" /><br/>

title_main: {{ title_main }}<br/>
title_budget: {{ title_budget }}<br/>
title_main_categories: {{ title_main_categories }}<br/>
title_variations: {{ title_variations }}<br/>
title_account: {{ title_account }}<br/>
title_unit: {{ title_unit }}<br/>
title_advice: {{ title_advice }}<br/>
title_portfolio: {{ title_portfolio }}<br/>
title_highlighted: {{ title_highlighted }}<br/>
title_networth: {{ title_networth }}<br/>
title_annual_spending: {{ title_annual_spending }}<br/>
title_personal_finance_score: {{ title_personal_finance_score }}<br/>

msg_no_variation: {{ msg_no_variation|safe }}<br/>
msg_no_scheduled: {{ msg_no_scheduled|safe }}<br/>
msg_no_highlighted: {{ msg_no_highlighted|safe }}<br/>
msg_no_budget: {{ msg_no_budget|safe }}<br/>
msg_no_share: {{ msg_no_share|safe }}<br/>
msg_amount_unit_date: {{ msg_amount_unit_date|safe }}<br/>

about_welcome: {{ about_welcome|safe }}<br/>
about_programname: {{ about_programname|safe }}<br/>
about_version: {{ about_version|safe }}<br/>
about_bugaddress: {{ about_bugaddress|safe }}<br/>
about_copyrightstatement: {{ about_copyrightstatement|safe }}<br/>
about_homepage: {{ about_homepage|safe }}<br/>
about_forumpage: {{ about_forumpage|safe }}<br/>
about_newspage: {{ about_newspage|safe }}<br/>
about_operationpage: {{ about_operationpage|safe }}<br/>
about_accountpage: {{ about_accountpage|safe }}<br/>
about_shortdescription: {{ about_shortdescription|safe }}<br/>
about_othertext: {{ about_othertext|safe }}<br/>
about_maintext: {{ about_maintext|safe }}<br/>
about_did_you_know: {{ about_did_you_know|safe }}<br/>
  </div>
</div>  

  <h2>The report:</h2>  
From this object you can have access to more complex values:
<div class="panel panel-primary">
  <div class="panel-heading">Code</div>
  <div class="panel-body">
report.period=&#123;{ report.period }}<br/>
report.previous_period=&#123;{ report.previous_period }}<br/>
report.previous.period=&#123;{ report.previous.period }}<br/>
report.previous.previous_period=&#123;{ report.previous.previous_period }}<br/>
report.tip_of_day;{ report.tip_of_day }}<br/>


report.budget_table=&#123;{ report.budget_table|dump|safe }}<br/>
report.unit_table=&#123;{ report.unit_table|dump|safe }}<br/>
report.portfolio=&#123;{ report.portfolio|dump|safe }}<br/>
report.account_table=&#123;{ report.account_table|dump|safe }}<br/>
report.bank_table=&#123;{ report.bank_table|dump|safe }}<br/>
report.scheduled_operations=&#123;{ report.scheduled_operations|dump|safe }}<br/>
report.categories_period=&#123;{ report.categories_period|dump|safe }}<br/>
report.categories_previous_period=&#123;{ report.categories_previous_period|dump|safe }}<br/>
report.income_vs_expenditure=&#123;{ report.income_vs_expenditure|dump|safe }}<br/>
report.networth=&#123;{ report.networth|money|safe }}<br/>
report.annual_spending=&#123;{ report.annual_spending|money|safe }}<br/>
report.personal_finance_score=&#123;{ report.personal_finance_score }}<br/>
report.categories_variations=&#123;{ report.categories_variations|dump|safe }}<br/>
report.categories_variations_issues=&#123;{ report.categories_variations_issues|dump|safe }}<br/>
  </div>
</div>
  
<div class="panel panel-primary">
  <div class="panel-heading">Result</div>
  <div class="panel-body">
report.period={{ report.period }}<br/>
report.previous_period={{ report.previous_period }}<br/>
report.previous.period={{ report.previous.period }}<br/>
report.previous.previous_period={{ report.previous.previous_period }}<br/>
report.tip_of_day={{ report.tip_of_day }}<br/>

report.budget_table={{ report.budget_table|dump|safe }}<br/>
report.unit_table={{ report.unit_table|dump|safe }}<br/>
report.portfolio={{ report.portfolio|dump|safe }}<br/>
report.account_table={{ report.account_table|dump|safe }}<br/>
report.bank_table={{ report.bank_table|dump|safe }}<br/>
report.scheduled_operations={{ report.scheduled_operations|dump|safe }}<br/>
report.categories_period={{ report.categories_period|dump|safe }}<br/>
report.categories_previous_period={{ report.categories_previous_period|dump|safe }}<br/>
report.income_vs_expenditure={{ report.income_vs_expenditure|dump|safe }}<br/>
report.networth={{ report.networth|money|safe }}<br/>
report.annual_spending={{ report.annual_spending|money|safe }}<br/>
report.personal_finance_score={{ report.personal_finance_score }}<br/>
report.categories_variations={{ report.categories_variations|dump|safe }}<br/>
report.categories_variations_issues={{ report.categories_variations_issues|dump|safe }}<br/>
  </div>
</div> 

  <h2>The document:</h2>  
From this object you can have access to more complex values:
<div class="panel panel-primary">
  <div class="panel-heading">Code</div>
  <div class="panel-body">
&#123;{ document|dump|safe }}
  </div>
</div>
<div class="alert alert-success" role="alert">The "dump" filter allows to dump the object</div>
  
<div class="panel panel-primary">
  <div class="panel-heading">Result</div>
  <div class="panel-body">
{{ document|dump|safe }}
  </div>
</div>  

 <h1>The filters</h1>
  In addition of standard filters, you can the following filters developed for Skrooge.
<div class="panel panel-primary">
  <div class="panel-heading">Code</div>
  <div class="panel-body">
&#123;{ 10.1|money|safe }}<br/>
&#123;{ -10.2|money|safe }}<br/>
&#123;{ -10.3|money:"nocolor" }}<br/>
&#123;{ -10.4|money:"nodecimal" }}<br/>
&#123;{ -10.5|money:"nocolor;nodecimal" }}<br/>

&#123;{ 10.1|money:"1"|safe }}<br/>
&#123;{ -10.2|money:"1"|safe }}<br/>
&#123;{ -10.3|money:"1;nocolor" }}<br/>
&#123;{ -10.4|money:"1;nocolor;nodecimal" }}<br/>

&#123;{ 10.1|money:"2"|safe }}<br/>
&#123;{ -10.2|money:"2"|safe }}<br/>
&#123;{ -10.3|money:"2;nocolor" }}<br/>
&#123;{ -10.4|money:"2;nocolor;nodecimal" }}<br/>

&#123;{ 10.1|percent|safe }}<br/>
&#123;{ -10.2|percent|safe }}<br/>

&#123;{ 70000|filesizeformat }}<br/>

&#123;{ about_did_you_know|replace:"Did;Do" }}<br/>
  </div>
</div>
<div class="alert alert-success" role="alert">The "money" filter displays a numerical value as a money. With "1", in primary money. With "2", in secondary money</div>
<div class="alert alert-success" role="alert">The "percent" filter displays a numerical value as a percent</div>
<div class="alert alert-success" role="alert">The "filesizeformat" filter displays a numerical value as a file size format</div>
<div class="alert alert-success" role="alert">The "replace" filter replaces a string by another one</div>

<div class="panel panel-primary">
  <div class="panel-heading">Result</div>
  <div class="panel-body">
{{ 10.1|money|safe }}<br/>
{{ -10.2|money|safe }}<br/>
{{ -10.3|money:"nocolor" }}<br/>
{{ -10.4|money:"nodecimal" }}<br/>
{{ -10.5|money:"nocolor;nodecimal" }}<br/>

{{ 10.1|money:"1"|safe }}<br/>
{{ -10.2|money:"1"|safe }}<br/>
{{ -10.3|money:"1;nocolor" }}<br/>
{{ -10.4|money:"1;nocolor;nodecimal" }}<br/>

{{ 10.1|money:"2"|safe }}<br/>
{{ -10.2|money:"2"|safe }}<br/>
{{ -10.3|money:"2;nocolor" }}<br/>
{{ -10.4|money:"2;nocolor;nodecimal" }}<br/>

{{ 10.1|percent|safe }}<br/>
{{ -10.2|percent|safe }}<br/>

{{ 70000|filesizeformat }}<br/>

{{ about_did_you_know|replace:"Did;Do" }}<br/>
  </div>
</div>  

The following code will display the list of opened accounts with current amount:<br/>
<div class="panel panel-primary">
  <div class="panel-heading">Code</div>
  <div class="panel-body">
&#123;% for item in document|table:"v_account_display,t_close='N' ORDER BY t_name" %}<br/>
&#123;{ item|att:"rd_bank_id.t_name" }}.&#123;{ item|att:"t_name" }}:&#123;{ item|att:"f_CURRENTAMOUNT"|money|safe }}&lt;br/&gt;<br/>
&#123;% endfor %} 
  </div>
</div>
<div class="alert alert-success" role="alert">The "table" filter allows to search objects</div>
<div class="alert alert-success" role="alert">The "att" filter allows to get an attribute of an object</div>

<div class="panel panel-primary">
  <div class="panel-heading">Result</div>
  <div class="panel-body">
{% for item in document|table:"v_account_display,t_close='N' ORDER BY t_name" %}
{{ item|att:"rd_bank_id.t_name" }}.{{ item|att:"t_name" }}:{{ item|att:"f_CURRENTAMOUNT"|money|safe }}<br/>
{% endfor %} 
  </div>
</div>

The following code will execute a sql order and display the result:<br/>
<div class="panel panel-primary">
  <div class="panel-heading">Code</div>
  <div class="panel-body">
&#123;% for item in document|query:"select t_name as name, (select count(1) from operation where rd_account_id=account.id) as id from account" %}<br/>
&#123;{ item.0 }}:&#123;{ item.1 }}&lt;br/&gt;<br/> 
&#123;% endfor %} 
  </div>
</div>
<div class="alert alert-success" role="alert">The "query" filter allows to execute the sql order you want</div>

<div class="panel panel-primary">
  <div class="panel-heading">Result</div>
  <div class="panel-body">
{% for item in document|query:"select t_name as name, (select count(1) from operation where rd_account_id=account.id) as id from account" %}
{{ item.0 }}:{{ item.1 }}<br/>
{% endfor %} 
  </div>
</div>

  <h1>Some examples</h1>
To go faster, you can include already existing part of template like this:
<div class="panel panel-primary">
  <div class="panel-heading">Code</div>
  <div class="panel-body">
&#123;% include "default/personal_finance_score.html" %}<br/>
&#123;% include "default/income_vs_expenditure_table.html" %}<br/>
&#123;% include "default/budget_table.html" %}<br/>
&#123;% include "default/categories_previous_period_table.html" %}<br/>
&#123;% include "default/categories_period_table.html" %}<br/>
&#123;% include "default/categories_variations.html" %}<br/>
&#123;% include "default/bank_table.html" %}<br/>
&#123;% include "default/account_table.html" %}<br/>
&#123;% include "default/unit_table.html" %}<br/>
&#123;% include "default/portfolio.html" %}<br/>
&#123;% include "default/highlighted_operations.html" %}<br/>
  </div>
</div>   
<div class="panel panel-primary">
  <div class="panel-heading">Result</div>
  <div class="panel-body">
{% include "default/personal_finance_score.html" %}
{% include "default/income_vs_expenditure_table.html" %}
{% include "default/budget_table.html" %}
{% include "default/categories_previous_period_table.html" %}
{% include "default/categories_period_table.html" %}
{% include "default/categories_variations.html" %}
{% include "default/bank_table.html" %}
{% include "default/account_table.html" %}
{% include "default/unit_table.html" %}      
{% include "default/portfolio.html" %}  
{% include "default/highlighted_operations.html" %}     
  </div>
</div> 

You can also use external libraries to display values as you want:
<div class="panel panel-primary">
  <div class="panel-heading">Code</div>
  <div class="panel-body">
  &lt;img src="https://chart.apis.google.com/chart?cht=bvs&chxs=0,&#123;{ color_normaltext }}|1,&#123;{ color_normaltext }}&chbh=100&chxt=x,y&chxr=1,0,&#123;{ report.income_vs_expenditure.4.3 }}&chf=bg,s,&#123;{ color_normalbackground }}&chco=&#123;{ color_negativetext }}|&#123;{ color_positivetext }}&chd=t:&#123;{ report.income_vs_expenditure.2.3 }},&#123;{ report.income_vs_expenditure.1.3 }}&chds=0,&#123;{ report.income_vs_expenditure.4.3 }}&chs=300x200&chl=&#123;{ document|display:"f_CURRENTAMOUNT_EXPENSE" }}|&#123;{ document|display:"f_CURRENTAMOUNT_INCOME" }}&chts=&#123;{ color_normaltext }}&chtt=&#123;{ document|display:"f_CURRENTAMOUNT_EXPENSE" }} vs &#123;{ document|display:"f_CURRENTAMOUNT_INCOME" }}|&#123;{ report.period }}"/>
  </div>
</div>   
<div class="panel panel-primary">
  <div class="panel-heading">Result</div>
  <div class="panel-body">
  <img src="https://chart.apis.google.com/chart?cht=bvs&chxs=0,{{ color_normaltext }}|1,{{ color_normaltext }}&chbh=100&chxt=x,y&chxr=1,0,{{ report.income_vs_expenditure.4.3 }}&chf=bg,s,{{ color_normalbackground }}&chco={{ color_negativetext }}|{{ color_positivetext }}&chd=t:{{ report.income_vs_expenditure.2.3 }},{{ report.income_vs_expenditure.1.3 }}&chds=0,{{ report.income_vs_expenditure.4.3 }}&chs=300x200&chl={{ document|display:"f_CURRENTAMOUNT_EXPENSE" }}|{{ document|display:"f_CURRENTAMOUNT_INCOME" }}&chts={{ color_normaltext }}&chtt={{ document|display:"f_CURRENTAMOUNT_EXPENSE" }} vs {{ document|display:"f_CURRENTAMOUNT_INCOME" }}|{{ report.period }}"/>
  </div>
</div> 
  
  </body>
</html>
