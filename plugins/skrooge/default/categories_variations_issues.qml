/***************************************************************************
 * SPDX-FileCopyrightText: 2021 S. MANKOWSKI stephane@mankowski.fr
 * SPDX-FileCopyrightText: 2021 G. DE BURE support@mankowski.fr
 * SPDX-License-Identifier: GPL-3.0-or-later
 ***************************************************************************/
import QtQuick 2.0
import QtQuick.Controls 1.0

Column {
    property var pixel_size: report==null ? 0 : report.point_size
    
    Repeater {
        model: report==null ? null : report.categories_variations_issues
        Label {
            text: modelData
            font.pixelSize: pixel_size
            horizontalAlignment: Text.AlignLeft
            verticalAlignment: Text.AlignVCenter
            wrapMode: Text.Wrap
        }
    }
}
