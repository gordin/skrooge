#***************************************************************************
#* SPDX-FileCopyrightText: 2021 S. MANKOWSKI stephane@mankowski.fr
#* SPDX-FileCopyrightText: 2021 G. DE BURE support@mankowski.fr
#* SPDX-License-Identifier: GPL-3.0-or-later
#***************************************************************************
MESSAGE( STATUS "..:: CMAKE PLUGIN_IMPORT_XHB ::..")

PROJECT(plugin_import_xhb)

LINK_DIRECTORIES (${LIBRARY_OUTPUT_PATH})

SET(skrooge_import_xhb_SRCS
	skgimportpluginxhb.cpp
)

ADD_LIBRARY(skrooge_import_xhb MODULE ${skrooge_import_xhb_SRCS})
TARGET_LINK_LIBRARIES(skrooge_import_xhb KF5::Parts KF5::Archive skgbasemodeler skgbasegui skgbankmodeler skgbankgui)

########### install files ###############
INSTALL(TARGETS skrooge_import_xhb DESTINATION ${KDE_INSTALL_QTPLUGINDIR})
INSTALL(FILES ${PROJECT_SOURCE_DIR}/org.kde.skrooge-import-xhb.desktop DESTINATION ${KDE_INSTALL_KSERVICES5DIR})

