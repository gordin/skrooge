#***************************************************************************
#* SPDX-FileCopyrightText: 2021 S. MANKOWSKI stephane@mankowski.fr
#* SPDX-FileCopyrightText: 2021 G. DE BURE support@mankowski.fr
#* SPDX-License-Identifier: GPL-3.0-or-later
#***************************************************************************
MESSAGE( STATUS "..:: CMAKE PLUGIN_IMPORT_OFX ::..")

PROJECT(plugin_import_ofx)

MESSAGE( STATUS "OFX found. OFX support enabled")

INCLUDE(KDECompilerSettings NO_POLICY_SCOPE)
KDE_ENABLE_EXCEPTIONS()

LINK_DIRECTORIES (${LIBRARY_OUTPUT_PATH})

SET(skrooge_import_ofx_SRCS
	skgimportpluginofx.cpp
)

ADD_LIBRARY(skrooge_import_ofx MODULE ${skrooge_import_ofx_SRCS})
TARGET_LINK_LIBRARIES(skrooge_import_ofx KF5::Parts ${LIBOFX_LIBRARIES} skgbasemodeler skgbasegui skgbankmodeler skgbankgui)

########### install files ###############
INSTALL(TARGETS skrooge_import_ofx DESTINATION ${KDE_INSTALL_QTPLUGINDIR})
INSTALL(FILES ${PROJECT_SOURCE_DIR}/org.kde.skrooge-import-ofx.desktop DESTINATION ${KDE_INSTALL_KSERVICES5DIR})

